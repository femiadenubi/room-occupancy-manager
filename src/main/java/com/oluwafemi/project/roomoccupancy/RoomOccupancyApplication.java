package com.oluwafemi.project.roomoccupancy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RoomOccupancyApplication {

	public static void main(String[] args) {
		SpringApplication.run(RoomOccupancyApplication.class, args);
	}

}
