# README #

### About The Project ###

This is a room occupancy REST API build with the Spring Boot framework, for a hotel to optimize rooms allocation to its clients.

It exposes a single endpoint `GET /occupancy?payments={comma-separated-integer-payment-amounts}`

It returns an optimal allocation of rooms in 2 room categories (Premium and Economy) and the total amount of money from the allocation.

#### Built With ####
* Spring Boot Framework
* Maven
* Java 11

### Build JAR ###

To run all tests and build the application, run **`mvn clean package`**

### Contact ###

Oluwafemi Adenubi - femiadenubi@gmail.com